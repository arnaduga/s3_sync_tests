## FIRST BUCKET  
    region-1 = "eu-west-1"
    s3-1-bucketName = "poc-two-ways-sync-multiregion-source"

## SECOND BUCKET  
    region-2 = "ap-southeast-1"
    s3-2-bucketName = "poc-two-ways-sync-multiregion-destination"

## Tags to be added
    default_tags = {
        environment = "poc"
        application = "s3-sync-tests"
        lifetime = "Remove under 5 days"
    }

## Temporary file to be created and cleaned
    tempFilename = "/tmp/myReplicationSetup.json"
    uploadFilename = "/tmp/myTestFile"

## Lambda function name
    lambda-functionName = "poc-2waysync-lambdaFunction"
