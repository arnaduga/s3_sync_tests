variable "region-1" {
  description = "AWS region to host your network"
  default     = "eu-west-1"
}

variable "region-2" {
  description = "AWS region to host your network"
  default     = "ca-central-1"
}

variable "default_tags" {
  type = "map"
  default = {
    environment = "poc"
	  application = "s3-sync-tests"
  }
}
variable "tempFilename" {
  description = "Absolute path to temporary file"
  default = "/tmp/replication.json" 
}

variable "s3-1-bucketName" {
  description = "S3 bucket name to be created in region 1 (Europe)"
  default = "poc-2waysync-source"
}

variable "s3-2-bucketName" {
  description = "S3 bucket name to be created in region 2"
  default = "poc-2waysync-destination"
}

variable "uploadFilename" {
  description = "Small file containing current time to be uploaded as test"
  default = "/tmp/myTestFile"
}

variable "lambda-functionName" {
  description = "Lamdba name to be created in region 1"
  default = "poc-2waysync-lambdaFunction"
}
