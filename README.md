# s3_sync_tests

## Goal

The purpose of these script is to setup an AWS environment to perform some tests about **_Two-ways sync_** between two distant S3 buckets.

## Pre-requisites

### AWS CLI

The AWS CLI must be installed

To check if it is correct, type `aws --version` in a command line window. You should have something similar to:

```bash
arnaud@host:~$ aws --version
aws-cli/1.16.241 Python/3.7.3 Linux/4.19.0-6-amd64 botocore/1.12.231
arnaud@host:~$
```

Please refer to the AWS CLI documentation to set up AWS credentials (in a local file or as environment variables).

### Terraform

The Terraform version >=0.12 is required.

To check your version, type `terraform --version` in a command line window. You should have something similar to:

```bash
arnaud@host:~$ terraform --version
Terraform v0.12.9
arnaud@host:~$
```

## Create the infrastructure

### Update variables

Edit the file `variables.tfvars` and adjust the different variables.

```
## FIRST BUCKET  
    region-1 = "eu-west-1"
    s3-1-bucketName = "poc-two-ways-sync-source"

## SECOND BUCKET  
    region-2 = "us-east-1"
    s3-2-bucketName = "poc-two-ways-sync-destination"

## Tags to be added
    default_tags = {
        environment = "poc"
        application = "s3-sync-tests"
        lifetime = "Remove under 5 days"
    }

## Temporary file to be created and cleaned
    tempFilename = "/tmp/myReplicationSetup.json"
    uploadFilename = "/tmp/myTestFile"

## Lambda function name
    lambda-functionName = "poc-2waysync-lambdaFunction"
```

Please note the script runs on Linux only (as there is a ```sleep``` command and path Linux way...).

### Run the script

Just launch `terraform apply -var-file=variables.tfvars` command, and confirm.

#### Possible error

If you receive this error:
```
Error: Error creating S3 bucket: AuthorizationHeaderMalformed: The authorization header is malformed; the region 'us-east-1' is wrong; expecting 'sa-east-1'
```

Please adjust the bucket name. The origin seems to be a DNS replication delay because of multiple creation/deletion, time to propagate DNS records and changing region.


## About the Lambda function

### Change the index.js content file

If you change the ```index.js``` code, you have to pack it again. In the project root folder, run `cd lambda_sources; zip ../lambda_function.zip index.js; cd ..`

### Run the Lambda

#### Via console

You can run the function by clicking on the **Test** button of the function. To do, so, you will need to create a test event. You can put whatever you want, it is not used.

#### Via CLI (preferred)

... or you can run ```aws lambda invoke --function-name <functionName> <outputFilename>.log``` to run it!

#### Lambda possible error

In case of `Lambda was unable to decrypt the environment variables due to an internal service error`, just add a space somewhere in the Lambda code and save it.

It is caused by multiple `terraform apply` and `terraform destroy`...



### How to read response


Here is an example of the full response:
```
{
  "AtoB": [
    {
      "file_size_in_bytes": 131072,
      "post_date": "2019-10-02T13:49:41.033Z",
      "get_date": "2019-10-02T13:49:45.292Z",
      "duration_in_ms": 4259
    },
    {
      "file_size_in_bytes": 262144,
      "post_date": "2019-10-02T13:49:45.656Z",
      "get_date": "2019-10-02T13:49:48.121Z",
      "duration_in_ms": 2465
    }
  ],
  "BtoA": [
    {
      "file_size_in_bytes": 131072,
      "post_date": "2019-10-02T13:50:15.790Z",
      "get_date": "2019-10-02T13:50:34.142Z",
      "duration_in_ms": 18352
    },
    {
      "file_size_in_bytes": 262144,
      "post_date": "2019-10-02T13:50:35.715Z",
      "get_date": "2019-10-02T13:50:59.042Z",
      "duration_in_ms": 23327
    }
  ]
}
```

It has 2 main sections: 

- ```AtoB``` that represenst information about the replication from first bucket to second bucket 
- ```BtoA``` that represenst information about the replication from second bucket to first bucket

Each section is an array of test like:
```
    {
      "file_size_in_bytes": 131072,
      "post_date": "2019-10-02T13:49:41.033Z",
      "get_date": "2019-10-02T13:49:45.292Z",
      "duration_in_ms": 4259
    }
```

- ```file_size_in_bytes```: number of byte of the read file (once replicated)
- ```post_date```: date and time when the file has been *posted* in source bucket (A or B based on test)
- ```get_date```: date and time when the file has been *found* from destination bucket (B or A based on test)
- ```duration_in_ms```: the difference in ms between the two previously mentionned dates

So, for the above message, we can see the file that weights 131072 bytes (the 128 Kb test) was replicated after 4.2 seconds.
