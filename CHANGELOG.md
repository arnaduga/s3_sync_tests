# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2019.10.02] v1.0.0

### Added
- ```iam.tf``` file to create the IAM role and policies
- ```s3.tf``` file to create the two S3 buckets
- ```addons.tf``` file to apply the Replication setup
- ```README.md``` file to list pre-requisites and how to setup guide
- ```lambda_sources``` NodeJS folder, for the Lambda source code
- ```lambda.tf``` for the Lambda creation by Terraform
- ```output.tf``` to display s3 bucket name and Lambda function name
- ```provider.tf``` to manage 2 different provider, one by region
- ```variables.tf``` and ```variables.tfvars``` to manage architecture creation variables