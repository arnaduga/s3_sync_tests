resource "aws_s3_bucket" "s3-1" {
  bucket = "${var.s3-1-bucketName}"
  tags = "${merge(var.default_tags)}"
  region = "${var.region-1}"
  versioning {
    enabled = true
  }  
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  force_destroy = true

  replication_configuration {
    role = "${aws_iam_role.replication-way1.arn}"

    rules {
      id     = "full"
      status = "Enabled"

      destination {
        bucket        = "${aws_s3_bucket.s3-2.arn}"
        storage_class = "STANDARD"
      }
    }
  }

  ##Create a config file
    provisioner "local-exec" {
      command = <<CMD
echo '{"Role": "${aws_iam_role.replication-way2.arn}","Rules":[{"DeleteMarkerReplication": { "Status": "Disabled" },"Status":"Enabled","Priority":1,"Filter":{"Prefix":""},"Destination":{"Bucket":"${self.arn}"}}]}' > ${var.tempFilename}
CMD
  }

}


resource "aws_s3_bucket" "s3-2" {
  bucket = "${var.s3-2-bucketName}"
  provider = "aws.remote"
  force_destroy = true
  tags = "${merge(var.default_tags)}"
  region = "${var.region-2}"
  versioning {
    enabled = true
  }
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }  
}


