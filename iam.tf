# ROLE for replication EU -> REMOTE (way1)
resource "aws_iam_role" "replication-way1" {
  name = "tf-iam-role-replication-way1"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

# POLICY for replication EU -> REMOTE (way1)
resource "aws_iam_policy" "replication-way1" {
  name = "tf-iam-role-policy-replication-way1"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.s3-1.arn}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.s3-1.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.s3-2.arn}/*"
    }
  ]
}
POLICY
}

# Attache POLICY to ROLE
resource "aws_iam_policy_attachment" "replication-way1" {
  name       = "tf-iam-role-attachment-replication-way1"
  roles      = ["${aws_iam_role.replication-way1.name}"]
  policy_arn = "${aws_iam_policy.replication-way1.arn}"
}

# ################################################################""

# ROLE for replication REMOTE -> EU (way2)
resource "aws_iam_role" "replication-way2" {
  name = "tf-iam-role-replication-way2"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

# POLICY for replication REMOTE -> EU (way2)
resource "aws_iam_policy" "replication-way2" {
  name = "tf-iam-role-policy-replication-way2"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.s3-2.arn}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.s3-2.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.s3-1.arn}/*"
    }
  ]
}
POLICY
}

# Attache POLICY to ROLE
resource "aws_iam_policy_attachment" "replication-way2" {
  name       = "tf-iam-role-attachment-replication-way2"
  roles      = ["${aws_iam_role.replication-way2.name}"]
  policy_arn = "${aws_iam_policy.replication-way2.arn}"
}






# ROLE and POLICY related to the Lambda function
resource "aws_iam_role" "iam_for_lambda" {
  name = "basic_execution_role_for_lambda"

  assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [{
			"Action": "sts:AssumeRole",
			"Principal": {
				"Service": "lambda.amazonaws.com"
			},
			"Effect": "Allow"
		}
  ]
}
EOF
}


resource "aws_iam_policy" "lambda_policy_s3_access" {
  name = "tf-iam-policy-replication-s3_access"
  policy = <<POLICY
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Effect": "Allow",
			"Action": [
				"logs:CreateLogGroup",
				"logs:CreateLogStream",
				"logs:PutLogEvents"
			],
			"Resource": "arn:aws:logs:*:*:*"
		},
		{
			"Effect": "Allow",
			"Action": ["s3:*"],
			"Resource": ["${aws_s3_bucket.s3-1.arn}", "${aws_s3_bucket.s3-2.arn}"]
		},
		{
			"Effect": "Allow",
			"Action": ["s3:*"],
			"Resource": "*"
		}
	]
}
POLICY
}

# Attache POLICY to ROLE
resource "aws_iam_policy_attachment" "replication-lambda" {
  name       = "tf-iam-role-attachment-replication-way2"
  roles      = ["${aws_iam_role.iam_for_lambda.name}"]
  policy_arn = "${aws_iam_policy.lambda_policy_s3_access.arn}"
}