resource "aws_lambda_function" "test_lambda" {
  filename      = "lambda_function.zip"
  function_name = "${var.lambda-functionName}"
  role          = "${aws_iam_role.iam_for_lambda.arn}"
  handler       = "index.handler"

  description = "Function to perform some replication tests"

  memory_size = 640
  timeout = 240

  source_code_hash = "${filebase64sha256("lambda_function.zip")}"

  runtime = "nodejs10.x"

  environment {
    variables = {
      bucket1 = "${var.s3-1-bucketName}"
      bucket2 = "${var.s3-2-bucketName}"
      numberOfTries = 200
      inBetweenTimeInMs = 100
      maxSize = 4096
    }
  }
}