provider "aws" {
  region  = "${var.region-1}"
}

provider "aws" {
  alias  = "remote"
  region = "${var.region-2}"
}

provider "null" {}

#terraform {
#  backend "s3" {
#    bucket = "arnaduga-terraform"
#    key    = "s3-sync"
#    region = "eu-west-1"
#  }
#}
