resource "null_resource" "s3bucket" {
  depends_on = ["aws_s3_bucket.s3-2","aws_s3_bucket.s3-1"]
  provisioner "local-exec" {
    command = "sleep 5 && aws s3api put-bucket-replication --bucket=${aws_s3_bucket.s3-2.bucket} --replication-configuration=file://${var.tempFilename}"
  }
}

resource "null_resource" "cleanup" {
  depends_on = ["null_resource.s3bucket"]
  provisioner "local-exec" {
    command = "rm ${var.tempFilename}"
  }
}

resource "null_resource" "post_test_data" {
  depends_on = ["null_resource.s3bucket","null_resource.cleanup"  ]
  provisioner "local-exec" {
    command = "sleep 10 && echo $(date '+%Y-%m-%d') > ${var.uploadFilename} && aws s3 cp ${var.uploadFilename} s3://${aws_s3_bucket.s3-1.bucket}/postedOnBucket1 && aws s3 cp ${var.uploadFilename} s3://${aws_s3_bucket.s3-2.bucket}/postedOnBucket2"
  }
}

resource "null_resource" "cleanupbis" {
  depends_on = ["null_resource.post_test_data"]
  provisioner "local-exec" {
    command = "rm ${var.uploadFilename}"
  }
}