const AWS = require('aws-sdk')
const s3 = new AWS.S3();
const setup = {
    numberOfTries: process.env.numberOfTries || 150,
    inBetweenTimeInMs: process.env.inBetweenTimeInMs || 100,
    maxSize: process.env.maxSize || 256
}

function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}

// Up to 32 Mbyte included ... not more
async function postFile( bucketName , filename, fileSizeInKBytes ) {
    return new Promise( (resolve, reject) => {

        console.log(`[postFile] Create a ${fileSizeInKBytes} Kbytes weight file....`)
        var fakeString = "";
        for (var i = 0 ; i < fileSizeInKBytes*1024 ; i++) fakeString+='a'

        var params = {
            Body: Buffer.from(fakeString, 'utf8'),
            Bucket: bucketName, 
            Key: filename
        };
        s3.putObject(params, function(err, data) {
            if (err) { reject(err) }
            else     { resolve( new Date() ) }
        });
    });
}


async function getHeadFile( bucketName , filename) {
    return new Promise ( (resolve, reject) =>  {

        var params = {
            Bucket: bucketName, 
            Key: filename
        };

        s3.headObject(params, function(err, data) {
            if (err) reject(err); 
            else     resolve(data);
        });
        
    });
}

async function tryToGetFile ( bucketName , filename ) {
    return new Promise ( async (resolve, reject) => {

        console.log(`[tryToGetFile] Loop ${setup.numberOfTries} times to check if ${filename} does exist...`)

        var essay = 0;
        var letsContinue = true
        var resp = {}

        while (letsContinue) {
            try {
                let data = await getHeadFile(bucketName, filename)
                resp = {
                    status: "found",
                    time: (new Date()),
                    size: data.ContentLength
                }
                letsContinue = false
            } catch (e) {
                essay++;
                await sleep(setup.inBetweenTimeInMs)
            }
            if (essay >= setup.numberOfTries) {
                letsContinue=false 
                resp = {
                    status: "failed",
                    description: "After a reasonnable number of attempts, nothing found. Sorry"
                }
                reject(resp)
            }
        }

        resolve( resp )

    });
}


exports.handler = async (event, context) => {

    var testFilename, testEnd, testStartDate;
    var testResults = {"AtoB": [], "BtoA": []};

    console.log(`[TEST RUN A -> B] Begin tests @ ${(new Date()).toISOString()}...`)
    for (var i = 128; i <= process.env.maxSize ; i = i*2) {
        testFilename = + new Date() + ".fakeFile";
        try {
            testStartDate = await postFile(process.env.bucket1,testFilename, i);
            testEnd = await tryToGetFile(process.env.bucket2,testFilename) ;
        } catch(e) {
            return {"status":"failed","description":e}
        }
        testResults.AtoB.push( { file_size_in_bytes : testEnd.size, post_date: testStartDate, get_date: testEnd.time, duration_in_ms : (testEnd.time - testStartDate) } )   
    }
    console.log(`[TEST RUN A->B] End tests @ ${(new Date()).toISOString()}...`)

    console.log(`[TEST RUN B -> A] Begin tests @ ${(new Date()).toISOString()}...`)
    for (var i = 128; i <= process.env.maxSize ; i = i*2) {
        testFilename = + new Date() + ".fakeFile";
        try {
            testStartDate = await postFile(process.env.bucket2,testFilename, i);
            testEnd = await tryToGetFile(process.env.bucket1,testFilename) ;
        } catch(e) {
            return {"status":"failed","description":e}
        }
        testResults.BtoA.push( { file_size_in_bytes : testEnd.size, post_date: testStartDate, get_date: testEnd.time, duration_in_ms : (testEnd.time - testStartDate) } )   
    }
    console.log(`[TEST RUN B->A] End tests @ ${(new Date()).toISOString()}...`)


    return testResults;
};