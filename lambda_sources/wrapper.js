const index = require('./index.js')

//Simulate the event
const event = {
}   

async function main() {
    //Simulate Lambda Variables
    process.env.bucket1 = "two-ways-sync-source"
    process.env.bucket2 = "two-ways-sync-destination"
    process.env.maxSize = 256
    var resp = await index.handler(event)
    console.log(resp);
}

main();
