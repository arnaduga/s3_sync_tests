output "s3_1" {
  value = "${aws_s3_bucket.s3-1.arn}"
}

output "s3_2" {
  value = "${aws_s3_bucket.s3-2.arn}"
}

output "lambda_functionName" {
  value = "${aws_lambda_function.test_lambda.function_name}"
}